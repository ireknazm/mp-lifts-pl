const webpack = require("webpack");
const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
  mode: "production",
  entry: {
    mainApp: ["./src/main.js"]
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "dist"),
    libraryTarget: "umd",
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  node: {
    fs: "empty"
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      "typeof window": JSON.stringify("object")
    })
  ],
  devtool: "source-map",
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
    modules: ["node_modules", "src", "static"],
    extensions: [
      ".webpack.js",
      ".web.js",
      ".ts",
      ".js",
      ".css",
      ".vue",
      ".png",
    ],
  },

  module: {
    rules: [
      {parser: {system: false}},
      {
        test: /\.vue?$/,
        loader: "vue-loader"
      },
      {
        test: /\.(jpe?g|gif|png|svg|ttf|eot|wav|mp3)$/,
        loader: "file-loader"
      },
      {
        test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
      {
        test: /\.s?css$/,
        use: ["vue-style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.svg$/,
        loader: 'vue-svg-loader', // `vue-svg` for webpack 1.x
      },
    ]
  }
};
