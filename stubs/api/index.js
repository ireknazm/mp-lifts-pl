const fs = require("fs");
const path = require("path");
const workflow = require("./workflow");

const router = require("express").Router();

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

router.get("/workflow", (request, response) => {
  const { cmd, name } = request.query;
  let flowName = "";
  let stateName = "";
  let data = {};

  if (cmd === "start") {
    if (request.session.workflow) {
      flowName = request.session.workflow.flowName;
      stateName = request.session.workflow.stateName;
      data = request.session.workflow.data;
    } else {
      flowName = name;
      stateName = workflow.flows[flowName].init;
      data = workflow.flows[flowName].data;
    }
  } else if (cmd === "event") {
    flowName = request.session.workflow.flowName;

    const eventObject =
      workflow.flows[flowName].states[request.session.workflow.stateName]
        .events[name];
    data =
      workflow.flows[flowName].states[request.session.workflow.stateName].data;

    flowName = eventObject.newFlow || flowName;
    if (request.session.workflow.flowName !== flowName) {
      stateName = workflow.flows[flowName].init;
    } else {
      stateName = eventObject.newState || request.session.workflow.stateName;
    }
  }
  request.session.workflow = { stateName, flowName, data };
  response.send({ flowName, stateName, data });
});

const loadJson = (filepath, encoding = "utf8") =>
  JSON.parse(
    fs.readFileSync(path.resolve(__dirname, `${filepath}.json`), { encoding })
  );

router.get("/landing", (req, res) => {
  setTimeout(() => {
    res.send(loadJson("./landing"));
  }, 1000);
});

module.exports = router;

router.get("/cards", (request, response) => {
  setTimeout(() => {
    response.send(loadJson("./cards"));
  }, 1000);
});

module.exports = router;

router.get("/districts", (request, response) => {
  setTimeout(() => {
    response.send(loadJson("./districts"));
  }, 1000);
});

module.exports = router;
