const pkg = require("./package.json");
const webpack = require("./webpack.develop.js");
module.exports = {
  apiPath: "stubs/api",
  webpackConfig: webpack,
  apps: {
    "elevator":{
      "version": process.env.VERSION || pkg.version,
      "name": "elevator"
    }
  },
  config: {
    "main.api.base.url": "/api",
    "elevator.api.base.url": "/api",
  },
  navigations: {
    news: "/news",
    orgs: "/orgs",
    transport: "/transport",
    elevator: "/elevator",
    ya: "https://yandex.ru",
  },
};
