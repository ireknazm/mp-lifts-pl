const webpack = require("webpack");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const pkg = require("./package.json");

const srcPath = path.resolve(__dirname, "src");
const stylesPath = path.resolve("node_modules/@mplt/ui-kit/src/styles");
const assetsPath = path.resolve("node_modules/@mplt/ui-kit/src/assets");
const packageName = pkg.name.slice(pkg.name.indexOf("-") + 1);
const outputDirectory = "dist";
console.log(111, process.env.VERSION);
module.exports = {
  mode: "development",
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, outputDirectory),
    libraryTarget: "umd",
    publicPath: `/static/${packageName}/${process.env.VERSION || pkg.version}/`,
    globalObject: `(typeof self !== 'undefined' ? self : this)`,
    hotUpdateChunkFilename: ".hot/[id].[hash].hot-update.js",
    hotUpdateMainFilename: ".hot/[hash].hot-update.json",
  },
  node: {
    fs: "empty",
  },
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      "typeof window": JSON.stringify("object"),
    }),
  ],
  devtool: "#source-map",
  resolve: {
    modules: ["node_modules", "src"],
    alias: {
      vue: "vue/dist/vue.esm.js",
      images: "static/images",
      "@": srcPath,
      styles: stylesPath,
      assets: assetsPath,
    },
    extensions: [".webpack.js", ".web.js", ".js", ".css", ".vue"],
  },

  module: {
    rules: [
      { parser: { system: false } },
      {
        test: /\.vue?$/,
        loader: "vue-loader",
      },
      {
        test: /\.(jpe?g|gif|png|woff|ttf|eot|wav|mp3)$/,
        loader: "file-loader",
        options: {
          name: "[path]/[name].[ext]",
          esModule: false,
        },
      },
      {
        test: /\.svg$/,
        loader: "vue-svg-loader", // `vue-svg` for webpack 1.x
      },
      {
        test: /\.s?css$/,
        use: [
          "vue-style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              prependData: `@import 'src/globals.scss';`,
              sassOptions: {
                includePaths: [path.resolve(__dirname, "../src/")],
              },
            },
          },
        ],
      },
      {
        test: "/.js?$/",
        loader: "babel-loader",
      },
    ],
  },
};
