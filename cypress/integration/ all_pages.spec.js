describe("Go through all pages", () => {
    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.visit("http://localhost:8099/elevator/");
        cy.scrollTo('top')
    });

    it("Go forward through app", () => {
        cy.server();
        cy.route("/api/*").as("api");
        cy.contains("Попробуй сейчас").click();
        cy.contains("Далее").click();
        cy.wait("@api");
        cy.contains("Далее").click();
    });

    it("Go forward through app", () => {
        cy.contains("Попробуй сейчас").click();
        cy.contains("Далее").click();
        cy.contains("Отправить заявку").click();
    });
});