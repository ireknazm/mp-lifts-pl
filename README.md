# Mediapult: Lifts part

## Prerequisites

You need to install following programs:

- **Node.js** LTS 

## Installation
1. `git clone https://bitbucket.org/ireknazm/mp-lifts-pl.git`
2. `git clone https://bitbucket.org/advancedjs2020/ui-kit.git`
3. `cd mp-lifts-pl`
4. `yarn`
5. `yarn start`