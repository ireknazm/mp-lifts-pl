import Vue from 'vue';
import App from './App.vue';

export default new Vue(App);

export const mount = component => {
  component.$mount("#app");
};

export const unmount = () => {
  new empty().$mount("#app");
};
