// jest.config.js
module.exports = {
  roots: ["<rootDir>/__tests__/"],
  setupFiles: ["<rootDir>/node_modules/regenerator-runtime/runtime"],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{js,vue}", "!**/src/main.js"],
  coverageDirectory: "<rootDir>/reports/coverage",
  coverageReporters: ["html"],
  coverageThreshold: {
    global: {
      statements: 80,
    },
  },
  moduleNameMapper: {
    "\\.(css|scss)": "identity-obj-proxy",
    "\\.(svg|png|jpg|jpeg)": "<rootDir>/__mocks__/filemock.js",
    "@ijl.cli": "<rootDir>/__mocks__/cli.js",
    "^@/(.*)$": "<rootDir>/src/$1",
  },
  testRegex: "(/__tests__/.*|(\\.|/))+(test|spec)\\.(js)?$",
  snapshotSerializers: ["jest-serializer-vue"],
  moduleFileExtensions: ["js", "json", "vue"],
  transformIgnorePatterns: ["/node_modules/(?!@mplt/ui-kit).+$"],
  transform: {
    "^.+\\.js$": "<rootDir>/node_modules/babel-jest",
    ".*\\.(vue)$": "vue-jest",
  },
  globals: {
    __webpack_public_path__: "",
  },
};
