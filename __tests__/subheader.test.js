import SubHeader from "@/components/SubHeader";
import { mount } from "@vue/test-utils";

describe("SubHeader", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(SubHeader, {
      propsData: {
        cardStatus: "cost",
      },
    });
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("subheader tab click", () => {
    wrapper.find(".active").trigger("click");
  });
});
