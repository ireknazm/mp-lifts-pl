import ListOfCards from "@/components/ListOfCards";
import { mount } from "@vue/test-utils";

describe("ListOfCards", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(ListOfCards);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});