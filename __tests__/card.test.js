import Card from "@/components/Card";
import { mount } from "@vue/test-utils";

describe("Card", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Card);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});