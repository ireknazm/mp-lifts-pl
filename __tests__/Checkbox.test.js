import Checkbox from "@/components/Checkbox";
import { mount } from "@vue/test-utils";

describe("Checkbox", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(Checkbox);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("oncheck", () => {
    wrapper.find("input").trigger("click");
  });
});
