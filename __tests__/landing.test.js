import Landing from "@/views/Landing.vue";
import { mount } from "@vue/test-utils";

describe("Landing", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Landing);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});