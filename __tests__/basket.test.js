import Basket from "@/views/Basket.vue";
import { mount } from "@vue/test-utils";

describe("Basket", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Basket);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});