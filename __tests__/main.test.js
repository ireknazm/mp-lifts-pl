import Main from "@/views/Main.vue";
import { mount } from "@vue/test-utils";

describe("Main", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(Main);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
