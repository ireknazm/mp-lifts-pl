import SendReq from "@/components/SendReq";
import { mount } from "@vue/test-utils";

describe("SendReq", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(SendReq);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});