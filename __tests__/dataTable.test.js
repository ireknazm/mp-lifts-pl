import DataTable from "@/components/DataTable";
import { mount } from "@vue/test-utils";

describe("DataTable", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(DataTable);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});