import App from "@/App.vue";
import { mount } from "@vue/test-utils";

describe("App", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(App);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
