import MainFilter from "@/components/MainFilter";
import { mount } from "@vue/test-utils";

describe("MainFilter", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(MainFilter);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});