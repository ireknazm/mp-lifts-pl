import BasketTotal from "@/components/BasketTotal";
import { mount } from "@vue/test-utils";

describe("BasketTotal", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(BasketTotal);
    });

    it("render test", () => {
        expect(wrapper).toMatchSnapshot();
    });
});